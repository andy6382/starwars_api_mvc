﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PlattSampleApp.Models;
using PlattSampleApp.Processors;

namespace PlattSampleApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStarWarsProcessor _starWarsProcessor;
        public HomeController(IStarWarsProcessor starWarsProcessor)
        {
            _starWarsProcessor = starWarsProcessor;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetAllPlanetsAsync()
        {
            AllPlanetsViewModel model;
            //adding try-catch in case something goes wrong, we want to be able
            //to report the error message
            try
            {
                model = await _starWarsProcessor.GetAllPlanetsAsync();
            }
            catch (Exception e)
            {
                model = new AllPlanetsViewModel
                {
                    ErrorMessage = e.Message
                };
                //todo: log the errormessage into a log file

            }
            return View(model);
        }

        public async Task<ActionResult> GetPlanetTwentyTwoAsync(int planetid)
        {
            SinglePlanetViewModel model;
            try
            {
                model = await _starWarsProcessor.GetSinglePlanetAsync(planetid);
            }
            catch (Exception e)
            {
                model = new SinglePlanetViewModel()
                {
                    ErrorMessage = e.Message
                };
                //todo: log the errormessage into a log file

            }
            return View(model);
        }

        public async Task<ActionResult> GetResidentsOfPlanetNabooAsync(string planetname)
        {
            PlanetResidentsViewModel model;
            try
            {
                model = await _starWarsProcessor.GetResidentsFromPlanetAsync(planetname);
            }
            catch (Exception e)
            {
                model = new PlanetResidentsViewModel()
                {
                    ErrorMessage = e.Message
                };
                //todo: log the errormessage into a log file

            }
            return View(model);
        }


        public async Task<ActionResult> VehicleSummaryAsync()
        {
            VehicleSummaryViewModel model;
            try
            {
                model = await _starWarsProcessor.GetVehiclesSummaryAsync();
            }
            catch (Exception e)
            {
                model = new VehicleSummaryViewModel()
                {
                    ErrorMessage = e.Message
                };
                //todo: log the errormessage into a log file

            }
            return View(model);
        }


        public async Task<ActionResult> GetPeopleSmallerThanMeAsync(int height)
        {
            PeopleViewModel model;
            try
            {
                model = await _starWarsProcessor.GetPeopleSmallerThanMeAsync(height);
            }
            catch (Exception e)
            {
                model = new PeopleViewModel()
                {
                    ErrorMessage = e.Message
                };
                //todo: log the errormessage into a log file
            }
            return View(model);
        }
    }
}