﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Models
{
    public class PeopleViewModel
    {
        public PeopleViewModel()
        {
            Residents = new List<ResidentSummary>();
        }

        public List<ResidentSummary> Residents { get; set; }

        public string ErrorMessage { get; internal set; }
    }
}