﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Models
{
    public class SinglePlanetViewModel
    {
        public string Name { get; set; }

        [JsonProperty(PropertyName = "rotation_period")]
        public string LengthOfDay { get; set; }

        [JsonProperty(PropertyName = "orbital_period")]
        public string LengthOfYear { get; set; }

        public string Diameter { get; set; }

        public string Climate { get; set; }

        public string Gravity { get; set; }

        [JsonProperty(PropertyName = "surface_water")]
        public string SurfaceWaterPercentage { get; set; }

        public string Population { get; set; }

        public string ErrorMessage { get; internal set; }
    }
}