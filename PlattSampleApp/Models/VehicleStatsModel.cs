﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Models
{
    public class VehicleStatsModel
    {
        public string Name { get; set; }

        public string Manufacturer { get; set; }

        public string Cost_In_Credits { get; set; }


    }
}