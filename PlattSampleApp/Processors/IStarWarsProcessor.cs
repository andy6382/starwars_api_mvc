﻿using PlattSampleApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlattSampleApp.Processors
{
    public interface IStarWarsProcessor
    {
        Task<AllPlanetsViewModel> GetAllPlanetsAsync();

        Task<SinglePlanetViewModel> GetSinglePlanetAsync(int planetId);
        Task<PlanetResidentsViewModel> GetResidentsFromPlanetAsync(string planetname);
        Task<VehicleSummaryViewModel> GetVehiclesSummaryAsync();
        Task<PeopleViewModel> GetPeopleSmallerThanMeAsync(int height);
    }
}
