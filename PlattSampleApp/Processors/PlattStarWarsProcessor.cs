﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PlattSampleApp.Models;
using PlattSampleApp.Repositories;
using System.Threading.Tasks;

namespace PlattSampleApp.Processors
{
    public class PlattStarWarsProcessor : IStarWarsProcessor
    {
        private readonly IStarWarsRepository _starWarsRepository;
        public PlattStarWarsProcessor(IStarWarsRepository starWarsRepository)
        {
            _starWarsRepository = starWarsRepository;
        }



        public async Task<SinglePlanetViewModel> GetSinglePlanetAsync(int planetId)
        {
            //no negative values allowed for planet id
            if (planetId < 0)
                throw new Exception("Negative value is not allowed for planet id");
            //planet id should not be greater than 61
            if (planetId > 61)
                throw new Exception("PlanetID should not be larger than 61");

            SinglePlanetViewModel planet = await _starWarsRepository.GetSinglePlanetAsync(planetId);

            return planet;
        }



        public async Task<AllPlanetsViewModel> GetAllPlanetsAsync()
        {
            AllPlanetsViewModel allPlanetsViewModel = new AllPlanetsViewModel();

            var planets = await _starWarsRepository.GetAllPlanetsAsync();
            //order the planets by diameter
            planets.Sort(CompareByDiameter);
            allPlanetsViewModel.Planets = planets;

            //average the diameters
            allPlanetsViewModel.AverageDiameter = CalculateAverageDiameter(planets);
            return allPlanetsViewModel;

        }

        /// <summary>
        /// Calculates average diameter of the list of planets, ignoring any diameter
        /// that is "unknown"
        /// </summary>
        /// <param name="planets"></param>
        /// <returns></returns>
        public double CalculateAverageDiameter(List<PlanetDetailsViewModel> planets)
        {
            double planetsWithDiameter = 0; 
            int sumOfDiameters = 0;
            double avgDiameter = 0;
            
            foreach (var planet in planets)
            {
                int diameter;
                if (int.TryParse(planet.Diameter, out diameter))
                {
                    sumOfDiameters += diameter;
                    planetsWithDiameter++;
                }
            }
            avgDiameter = sumOfDiameters / planetsWithDiameter;
            return avgDiameter;
        }


        /// <summary>
        /// Returns 1 if planetA has smaller diameter than planetB, 
        /// -1 if larger diameter, 0 if they have the same diameter.
        /// </summary>
        /// <param name="planetA"></param>
        /// <param name="planetB"></param>
        /// <returns></returns>
        public int CompareByDiameter (PlanetDetailsViewModel planetA, PlanetDetailsViewModel planetB)
        {

            //Convert the string diameter into an integer so we can compare the integers.
            //If the diameter is "unknown", give it an int value of -100 so that
            //it is always last in the list (assuming no other diameters will be negative)
            int planetAdiameterInt = planetA.Diameter.Equals("unknown") ? -100 : int.Parse(planetA.Diameter);
            int planetBdiameterInt = planetB.Diameter.Equals("unknown") ? -100 : int.Parse(planetB.Diameter);

            if (planetAdiameterInt > planetBdiameterInt) return -1;
            if (planetAdiameterInt < planetBdiameterInt) return 1;
            else return 0;
        }

        /// <summary>
        /// Retrieves the resident summaries of the residents that are associated with
        /// the supplied "planetname" argument. These resident summaries will be ordered
        /// alphabetically
        /// </summary>
        /// <param name="planetname"></param>
        /// <returns>PlanetResidentsViewModel</returns>
        public async Task<PlanetResidentsViewModel> GetResidentsFromPlanetAsync(string planetname)
        {
            PlanetResidentsViewModel planetResidents = new PlanetResidentsViewModel();
            
            //first get the URL endpoints for each of the residents found on the given planet
            string[] residentsUrls = await _starWarsRepository.GetResidentsUrls4Planet(planetname);

            //make a request to each of those URLs and store each of the resident's summary
            foreach (var url in residentsUrls)
            {
                ResidentSummary residentSummary = await _starWarsRepository.GetResidentInfo(url);
                planetResidents.Residents.Add(residentSummary);
            }

            //order the resident summaries alphabetically by Name before returning
            planetResidents.Residents = planetResidents.Residents.OrderBy((x) => x.Name).ToList<ResidentSummary>();

            return planetResidents;
        }

        public async Task<VehicleSummaryViewModel> GetVehiclesSummaryAsync()
        {
            VehicleSummaryViewModel vehicleSummaryViewModel = new VehicleSummaryViewModel();

            //Get list of all vehicle stats from repository
            List<VehicleStatsModel> vehicleStats = await _starWarsRepository.GetVehicleSummaries("https://swapi.co/api/vehicles/");

            //Remove any stats that have cost="unknown"
            var vehicleStatsWithCost = OnlyKnownCost(vehicleStats); 

            //Use list of vehicle stats (all costs are known) to generate the list of manufacturer information
            var manufacturerInformation = VehicalStats2ManufacturerStats(vehicleStatsWithCost);

            //Order by vehicle count (highest to lowest), then by cost (highest to lowest)
            var orderedManufacturerInformation = manufacturerInformation
                .OrderByDescending(m => m.VehicleCount)
                .ThenByDescending(m => m.AverageCost).ToList();
            vehicleSummaryViewModel.Details = orderedManufacturerInformation;

            //obtain the number of manufacturers
            vehicleSummaryViewModel.ManufacturerCount = orderedManufacturerInformation.Count;

            //obtain the total number of vehicles
            vehicleSummaryViewModel.VehicleCount = vehicleStatsWithCost.Count;
            return vehicleSummaryViewModel;
        }


        public List<VehicleStatsModel> OnlyKnownCost(List<VehicleStatsModel> vehicleStats)
        {
            vehicleStats.RemoveAll((c) => c.Cost_In_Credits.Equals("unknown"));
            return vehicleStats;
        }


        /// <summary>
        /// Manufacturer stats will have name, average cost, and number of vehicles
        /// </summary>
        /// <param name="vehicleStats"></param>
        /// <returns></returns>
        public List<VehicleStatsViewModel> VehicalStats2ManufacturerStats(List<VehicleStatsModel> vehicleStats)
        {
            List<VehicleStatsViewModel> manufacturerStats = new List<VehicleStatsViewModel>();


            var manufacturerGroups = vehicleStats.GroupBy((s) => s.Manufacturer);
            foreach (var group in manufacturerGroups) 
            {
                //Add up the total cost of all the vehicles.
                //This will be used later to compute the average cost.
                int cost = 0;
                foreach (var vehicle in group)
                {
                    cost += Int32.Parse(vehicle.Cost_In_Credits);
                }
                
                //Now generate the new manufacturer information that we will eventually return
                VehicleStatsViewModel vehicleStatsViewModel = new VehicleStatsViewModel() {
                    ManufacturerName = group.Key,
                    VehicleCount = group.Count(),
                    AverageCost = cost/group.Count(),
                };
                manufacturerStats.Add(vehicleStatsViewModel);
            }

            return manufacturerStats;
        }

        public async Task<PeopleViewModel> GetPeopleSmallerThanMeAsync(int height)
        {
            PeopleViewModel people = new PeopleViewModel();

            //get all the people
            var allPeople = await _starWarsRepository.GetPeople("https://swapi.co/api/people/");

            //remove any unknown heights
            allPeople.RemoveAll(p => p.Height.Contains("unknown"));

            //filter only those that are shorter than me and put them in alphabetical order
            people.Residents = allPeople
                .Where(p => Int32.Parse(p.Height) < height)
                .OrderBy(p => p.Name).ToList();
            
            return people;
        }
    }

}