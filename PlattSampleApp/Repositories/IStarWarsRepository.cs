﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlattSampleApp.Models;

namespace PlattSampleApp.Repositories
{
    public interface IStarWarsRepository
    {
        Task<List<PlanetDetailsViewModel>> GetAllPlanetsAsync();
        Task<SinglePlanetViewModel> GetSinglePlanetAsync(int planetId);
        Task<string[]> GetResidentsUrls4Planet(string planetname);
        Task<ResidentSummary> GetResidentInfo(string url);
        Task<List<VehicleStatsModel>> GetVehicleSummaries(string url);
        Task<List<ResidentSummary>> GetPeople(string url);
    }
}
