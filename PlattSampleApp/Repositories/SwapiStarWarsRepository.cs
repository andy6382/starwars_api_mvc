﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;
using PlattSampleApp.Models;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace PlattSampleApp.Repositories
{
    
    public class SwapiStarWarsRepository : IStarWarsRepository
    {
        static HttpClient httpClient = new HttpClient();

        public async Task<List<PlanetDetailsViewModel>> GetAllPlanetsAsync()
        {
            var planets = new List<PlanetDetailsViewModel>();
            
            //grab the 61 planets
            for (int i = 1; i <= 61; i++)
            {
                string result = await httpClient.GetStringAsync("https://swapi.co/api/planets/" + i + "/");
                var tempPlanet = JsonConvert.DeserializeObject<PlanetDetailsViewModel>(result);
                planets.Add(tempPlanet);
            }
            
            return planets;

            
        }

        public async Task<List<ResidentSummary>> GetPeople(string url)
        {
            var people = new List<ResidentSummary>();
            string result = await httpClient.GetStringAsync(url);
            JObject jsonObject = JObject.Parse(result);
            var nextUrl = (string)jsonObject["next"];

            if (!string.IsNullOrWhiteSpace(nextUrl))
            {
                people = await GetPeople(nextUrl);
            }

            var peopleResults =
                from r in jsonObject["results"]
                select (string)r["url"];

            foreach (var personUrl in peopleResults)
            {
                string personJson = await httpClient.GetStringAsync(personUrl);
                ResidentSummary personStats = JsonConvert.DeserializeObject<ResidentSummary>(personJson);
                people.Add(personStats);
            }

            return people;
        }

        public async Task<ResidentSummary> GetResidentInfo(string url)
        {
            var residentSummary = new ResidentSummary();

            string result = await httpClient.GetStringAsync(url);
            residentSummary = JsonConvert.DeserializeObject<ResidentSummary>(result);

            return residentSummary;
        }

        public async Task<string[]> GetResidentsUrls4Planet(string planetname)
        {
            string[] urls;
            string result = await httpClient.GetStringAsync("https://swapi.co/api/planets/?search=" + planetname);
            
            //use json.newtonsoft to query the result for the URLs of the residents
            JObject jsonObject = JObject.Parse(result);
            JArray jArray = (JArray)jsonObject["results"][0]["residents"];
            urls = jArray.Select(c => (string)c).ToArray();

            return urls;
        }

        public async Task<SinglePlanetViewModel> GetSinglePlanetAsync(int planetId)
        {
            var planet = new SinglePlanetViewModel();

            string result = await httpClient.GetStringAsync("https://swapi.co/api/planets/" + planetId + "/");
            planet = JsonConvert.DeserializeObject<SinglePlanetViewModel>(result);

            return planet;
        }

        /// <summary>
        /// Grabs all the vehicles returned by the given URL. 
        /// Recursively grabs all vehicle information from any "next" element.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<List<VehicleStatsModel>> GetVehicleSummaries(string url)
        {
            var vehicles = new List<VehicleStatsModel>();
            string result = await httpClient.GetStringAsync(url);
            JObject jsonObject = JObject.Parse(result);
            var nextUrl = (string)jsonObject["next"];

            if (!string.IsNullOrWhiteSpace(nextUrl))
            {
                vehicles = await GetVehicleSummaries(nextUrl);
            }

            var vehicleResults =
                from r in jsonObject["results"]
                select (string)r["url"];

            foreach (var vehicleUrl in vehicleResults)
            {
                string vehicleJson = await httpClient.GetStringAsync(vehicleUrl);
                VehicleStatsModel vehicleStats = JsonConvert.DeserializeObject<VehicleStatsModel>(vehicleJson);
                vehicles.Add(vehicleStats);
            }

            return vehicles;
        }
    }
}