﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using PlattSampleApp.Controllers;
using PlattSampleApp.Processors;
using Moq;

namespace Tests
{
    [TestFixture]
    public class HomeControllerTests
    {


        [Test]
        public void GetAllPlanets_CatchesExceptions()
        {
            var moqStarWarsProcessor = new Mock<IStarWarsProcessor>();
            moqStarWarsProcessor.Setup(swp => swp.GetAllPlanetsAsync()).Throws(new Exception("Something went wrong"));

            HomeController controller = new HomeController(moqStarWarsProcessor.Object);
            Assert.DoesNotThrowAsync(async () =>
            {
                ActionResult result = await controller.GetAllPlanetsAsync();
            });
            
        }

        [Test]
        public async Task ActionGetAllPlanets_RunsProcessorGetAllPlanetsAsync()
        {
            var moqStarWarsProcessor = new Mock<IStarWarsProcessor>();
            moqStarWarsProcessor.Setup(swp => swp.GetAllPlanetsAsync()).Verifiable();

            HomeController controller = new HomeController(moqStarWarsProcessor.Object);
            ActionResult result = await controller.GetAllPlanetsAsync();
            moqStarWarsProcessor.Verify();

        }

        [Test]
        public void GetPlanetTwentyTwoAsync_CatchesExceptions()
        {
            var moqStarWarsProcessor = new Mock<IStarWarsProcessor>();
            moqStarWarsProcessor.Setup(swp => swp.GetSinglePlanetAsync(22)).Throws(new Exception("Something went wrong"));

            HomeController controller = new HomeController(moqStarWarsProcessor.Object);
            Assert.DoesNotThrowAsync(async () =>
            {
                ActionResult result = await controller.GetPlanetTwentyTwoAsync(22);
            });

        }

        [Test]
        public void GetResidentsOfPlanetNabooAsync_CatchesExceptions()
        {
            var moqStarWarsProcessor = new Mock<IStarWarsProcessor>();
            moqStarWarsProcessor.Setup(swp => swp.GetResidentsFromPlanetAsync("naboo")).Throws(new Exception("Something went wrong"));

            HomeController controller = new HomeController(moqStarWarsProcessor.Object);
            Assert.DoesNotThrowAsync(async () =>
            {
                ActionResult result = await controller.GetResidentsOfPlanetNabooAsync("naboo");
            });

        }

        [Test]
        public void VehicleSummaryAsync_CatchesExceptions()
        {
            var moqStarWarsProcessor = new Mock<IStarWarsProcessor>();
            moqStarWarsProcessor.Setup(swp => swp.GetVehiclesSummaryAsync()).Throws(new Exception("Something went wrong"));

            HomeController controller = new HomeController(moqStarWarsProcessor.Object);
            Assert.DoesNotThrowAsync(async () =>
            {
                ActionResult result = await controller.VehicleSummaryAsync();
            });

        }

        [Test]
        public async Task ActionVehicleSummaryAsync_RunsProcessorGetVehiclesSummaryAsync()
        {
            var moqStarWarsProcessor = new Mock<IStarWarsProcessor>();
            moqStarWarsProcessor.Setup(swp => swp.GetVehiclesSummaryAsync()).Verifiable();

            HomeController controller = new HomeController(moqStarWarsProcessor.Object);
            ActionResult result = await controller.VehicleSummaryAsync();
            moqStarWarsProcessor.Verify();

        }


    }
}
