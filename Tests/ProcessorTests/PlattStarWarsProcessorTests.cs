﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using PlattSampleApp.Controllers;
using PlattSampleApp.Processors;
using Moq;
using PlattSampleApp.Models;
using PlattSampleApp.Repositories;

namespace Tests.ProcessorTests
{
    [TestFixture]
    class PlattStarWarsProcessorTests
    {


        [Test]
        public void CompareByDiameter_PlanetAGreaterThanPlanetB_ReturnsNegative1()
        {
            var planetA = new PlanetDetailsViewModel { Diameter = "3" };
            var planetB = new PlanetDetailsViewModel { Diameter = "1" };

            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            int result = processor.CompareByDiameter(planetA, planetB);
            Assert.AreEqual(-1, result);
        }

        [Test]
        public void CompareByDiameter_PlanetALessThanPlanetB_Returns1()
        {
            var planetA = new PlanetDetailsViewModel { Diameter = "1" };
            var planetB = new PlanetDetailsViewModel { Diameter = "3" };

            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            int result = processor.CompareByDiameter(planetA, planetB);
            Assert.AreEqual(1, result);
        }
        [Test]
        public void CompareByDiameter_PlanetAEqualToPlanetB_Returns0()
        {
            var planetA = new PlanetDetailsViewModel { Diameter = "100" };
            var planetB = new PlanetDetailsViewModel { Diameter = "100" };

            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            int result = processor.CompareByDiameter(planetA, planetB);
            Assert.AreEqual(0, result);
        }



        [Test]
        public async Task GetAllPlanets_ReturnsOrderedListByDiameterAsync()
        {

            //arrange
            //create three Planets with different diameters
            var planet3 = new PlanetDetailsViewModel { Diameter = "3" };
            var planet1 = new PlanetDetailsViewModel { Diameter = "1" };
            var planet4 = new PlanetDetailsViewModel { Diameter = "4" };
            List<PlanetDetailsViewModel> planetList = new List<PlanetDetailsViewModel> { planet3, planet1, planet4 };

            //mock repository to return those three planets as a list
            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            moqStarWarsRepository.Setup(rep => rep.GetAllPlanetsAsync()).Returns(Task.FromResult(planetList));

            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);
            var planets = await processor.GetAllPlanetsAsync();

            StringAssert.AreEqualIgnoringCase("4", planets.Planets[0].Diameter);
            StringAssert.AreEqualIgnoringCase("3", planets.Planets[1].Diameter);
            StringAssert.AreEqualIgnoringCase("1", planets.Planets[2].Diameter);
        }

        [Test]
        public async Task GetAllPlanets_UnknownDiameters_ReturnsOrderedListUnkownDiametersLastAsync()
        {

            //arrange
            //create three Planets with different diameters
            var planetA = new PlanetDetailsViewModel { Diameter = "unknown" };
            var planetB = new PlanetDetailsViewModel { Diameter = "unknown" };
            var planetC = new PlanetDetailsViewModel { Diameter = "4" };
            List<PlanetDetailsViewModel> planetList = new List<PlanetDetailsViewModel> { planetA, planetB, planetC };

            //mock repository to return those three planets as a list
            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            moqStarWarsRepository.Setup(rep => rep.GetAllPlanetsAsync()).Returns(Task.FromResult(planetList));

            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);
            var planets = await processor.GetAllPlanetsAsync();

            StringAssert.AreEqualIgnoringCase("4", planets.Planets[0].Diameter);
            StringAssert.AreEqualIgnoringCase("unknown", planets.Planets[1].Diameter);
            StringAssert.AreEqualIgnoringCase("unknown", planets.Planets[2].Diameter);
        }


        [Test]
        public void CalculateAverageDiameter_ReturnsAverageDiameters_NoUnknowns()
        {

            //arrange
            //create three Planets with different diameters
            var planetA = new PlanetDetailsViewModel { Diameter = "10" };
            var planetB = new PlanetDetailsViewModel { Diameter = "10" };
            var planetC = new PlanetDetailsViewModel { Diameter = "1" };
            List<PlanetDetailsViewModel> planetList = new List<PlanetDetailsViewModel> { planetA, planetB, planetC };

            //mock repository to return those three planets as a list
            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            var average = processor.CalculateAverageDiameter(planetList);

            Assert.AreEqual(7, average);
        }

        [Test]
        public void CalculateAverageDiameter_ReturnsAverageDiameters_Unknowns()
        {

            //arrange
            //create three Planets with different diameters
            var planetA = new PlanetDetailsViewModel { Diameter = "unknown" };
            var planetB = new PlanetDetailsViewModel { Diameter = "10" };
            var planetC = new PlanetDetailsViewModel { Diameter = "5" };
            List<PlanetDetailsViewModel> planetList = new List<PlanetDetailsViewModel> { planetA, planetB, planetC };

            //mock repository to return those three planets as a list
            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            var average = processor.CalculateAverageDiameter(planetList);

            Assert.AreEqual(7.5, average);
        }

        [Test]
        public void GetSinglePlanet_NegativeNumber_ThrowsException()
        {
            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            Exception e = Assert.ThrowsAsync<Exception>(async () => 
            {
                SinglePlanetViewModel planet = await processor.GetSinglePlanetAsync(-1);
            });
            StringAssert.AreEqualIgnoringCase("Negative value is not allowed for planet id", e.Message);
        }

        [Test]
        public void GetSinglePlanet_PlanetIDGreaterThan61_ThrowsException()
        {
            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            Exception e = Assert.ThrowsAsync<Exception>(async () =>
            {
                SinglePlanetViewModel planet = await processor.GetSinglePlanetAsync(62);
            });
            StringAssert.AreEqualIgnoringCase("PlanetID should not be larger than 61", e.Message);
        }

        [Test]
        public void VehicalStats2ManufacturerStats_ReturnsListOfVehicleStatsViewModel()
        {
            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            var vehicleStatsViewModelList = processor.VehicalStats2ManufacturerStats(new List<VehicleStatsModel>());
            Assert.IsNotNull(vehicleStatsViewModelList);
        }

        [Test]
        public void OnlyKnownCost_ReturnsListOfVehicleStats_WithoutCosts()
        {

            //Create list of three vehicle stats and make sure that the "unknown" is removed
            var vehicleStats = new List<VehicleStatsModel>()
            {
                new VehicleStatsModel() {Cost_In_Credits="unknown"},
                new VehicleStatsModel() {Cost_In_Credits="55"},
                new VehicleStatsModel() {Cost_In_Credits="10"}
            };
            var moqStarWarsRepository = new Mock<IStarWarsRepository>();
            var processor = new PlattStarWarsProcessor(moqStarWarsRepository.Object);

            
            var vehicleStatsFiltered = processor.OnlyKnownCost(vehicleStats);

            Assert.AreEqual(2, vehicleStatsFiltered.Count);
            foreach (var item in vehicleStatsFiltered) 
            {
                StringAssert.DoesNotContain("unknown", item.Cost_In_Credits);
            }
        }
        






    }
}
