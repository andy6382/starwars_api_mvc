﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using PlattSampleApp.Controllers;
using PlattSampleApp.Processors;
using Moq;
using PlattSampleApp.Models;
using PlattSampleApp.Repositories;

namespace Tests.RepositoryTests
{
    [TestFixture]
    class SwapiStarWarsRepositoryTests
    {

        
        [Test]
        public void GetAllPlanets_ReturnsListOf_PlanetDetailsViewModel()
        {

            var repository = new SwapiStarWarsRepository();
            var planets = repository.GetAllPlanetsAsync();

            Assert.IsNotNull(planets);
        }

        

    }
}
